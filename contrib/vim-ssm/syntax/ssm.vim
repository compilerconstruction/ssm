" Vim syntax file
" " Language: SSM
" " Maintainer: Mart
" " Version: 1.0
" " Latest Revision: 2020-05-01

if exists("b:current_syntax")
	finish
endif

syn keyword ssmKeyword ldc add and div lsl lsr mod mul or rol ror sub xor eq ne lt gt le ge ajs bra brf brt bsr halt jsr lds ldms ldsa lda ldma ldaa ldc ldl ldml ldla link nop ret sts stms sta stma stl stml swp trap unlink ldh ldmh sth stmh ldr ldrr str swpr swprr neg not annote
syn keyword ssmKeyword LDC ADD AND DIV LSL LSR MOD MUL OR ROL ROR SUB XOR EQ NE LT GT LE GE AJS BRA BRF BRT BSR HALT JSR LDS LDMS LDSA LDA LDMA LDAA LDC LDL LDML LDLA LINK NOP RET STS STMS STA STMA STL STML SWP TRAP UNLINK LDH LDMH STH STMH LDR LDRR STR SWPR SWPRR NEG NOT ANNOTE
syn keyword ssmRegister SP MP HP RR R0 R1 R2 R3 R4 R5 R6 R7

syn match sssNumbers '[-]\(0x\x\+\|\d\+\)'
syn match ssmString '"\(\\.\?"\|[^"]\)*"'
syn match ssmLabel '[a-zA-Z0-9_-]\+:'

syn region ssmComment start="\(//\|;\)" end="$" contains=@Spell oneline display
syn region ssmString start=+"+ skip=+\\\\\|\\"+ end=+"+ oneline contains=@Spell

hi def link ssmLabel Label
hi def link ssmComment Comment
hi def link ssmString String
hi def link ssmNumber Number
hi def link ssmKeyword Keyword
hi def link ssmRegister Keyword
